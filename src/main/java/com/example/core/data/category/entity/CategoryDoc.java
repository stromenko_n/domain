package com.example.core.data.category.entity;

import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Data
public class CategoryDoc {
    @Id
    private ObjectId id;

    private String name;
    private String description;

    private ObjectId picId;
    private ObjectId parentId;
}
