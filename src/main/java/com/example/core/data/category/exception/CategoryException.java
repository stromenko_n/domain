package com.example.core.data.category.exception;

public class CategoryException extends Exception {

    public CategoryException(String message) {
        super(message);
    }
}
