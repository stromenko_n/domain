package com.example.core.data.category.model;

import com.example.core.data.category.entity.CategoryDoc;
import com.example.core.domain.utils.id.ObjIdUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.util.StringUtils;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CategoryDTO {
    private String id;

    private String name;
    private String description;

    private String picId;
    private String parentId;

    public static CategoryDTO of(CategoryDoc doc) {
        return new CategoryDTO(ObjIdUtils.objIdToString(doc.getId()),
                doc.getName(),
                doc.getDescription(),
                ObjIdUtils.objIdToString(doc.getPicId()),
                ObjIdUtils.objIdToString(doc.getParentId()));
    }

    public CategoryDoc toDoc() {
        CategoryDoc categoryDoc = new CategoryDoc();
        if (!StringUtils.isEmpty(id)) {
            categoryDoc.setId(new ObjectId(id));
        }
        categoryDoc.setName(name);
        categoryDoc.setDescription(description);
        if (!StringUtils.isEmpty(picId)) {
            categoryDoc.setPicId(new ObjectId(picId));
        }
        if (!StringUtils.isEmpty(parentId)) {
            categoryDoc.setParentId(new ObjectId(parentId));
        }

        return categoryDoc;
    }

    public ObjectId getObjId() {
        return new ObjectId(id);
    }
}
