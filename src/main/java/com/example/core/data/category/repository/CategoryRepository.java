package com.example.core.data.category.repository;

import com.example.core.data.category.entity.CategoryDoc;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

public interface CategoryRepository {

    String save(CategoryDoc doc);

    String delete(CategoryDoc doc);

    CategoryDoc findById(ObjectId id);

    List<CategoryDoc> findAll();

    List<CategoryDoc> findByCriteria(Query query);

    void dropCollection();

}
