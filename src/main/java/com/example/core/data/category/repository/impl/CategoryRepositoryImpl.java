package com.example.core.data.category.repository.impl;

import com.example.core.data.category.entity.CategoryDoc;
import com.example.core.data.category.model.CategoryDTO;
import com.example.core.data.category.repository.CategoryRepository;
import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@RequiredArgsConstructor
@Repository
public class CategoryRepositoryImpl implements CategoryRepository {
    private final MongoTemplate mongoTemplate;

    @Override
    public String save(CategoryDoc doc) {
        mongoTemplate.save(doc);
        return "OK!";
    }

    @Override
    public String delete(CategoryDoc doc) {
        mongoTemplate.remove(doc);
        return "OK!";
    }

    @Override
    public CategoryDoc findById(ObjectId id) {
        return mongoTemplate.findById(id, CategoryDoc.class);
    }

    @Override
    public List<CategoryDoc> findAll() {
        return mongoTemplate.findAll(CategoryDoc.class);
    }

    @Override
    public List<CategoryDoc> findByCriteria(Query query) {
        return mongoTemplate.find(query, CategoryDoc.class);
    }

    //Only tests
    @Override
    public void dropCollection() {
        mongoTemplate.dropCollection(CategoryDoc.class);
    }
}
