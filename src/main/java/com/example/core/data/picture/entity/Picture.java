package com.example.core.data.picture.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.BsonBinarySubType;
import org.bson.types.Binary;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Document
@Data
@NoArgsConstructor
public class Picture {
    @Id
    private ObjectId id;

    private String title;

    private Binary image;

    public static Picture of(String title, MultipartFile file) throws IOException {
        Picture picture = new Picture(title);
        picture.setImage(new Binary(BsonBinarySubType.BINARY, file.getBytes()));
        picture.setId(ObjectId.get());

        return picture;
    }

    Picture(String title) {
        this.title = title;
    }

}
