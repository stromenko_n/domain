package com.example.core.data.picture.exception;

public class PictureException extends Exception {

    public PictureException(String message) {
        super(message);
    }
}
