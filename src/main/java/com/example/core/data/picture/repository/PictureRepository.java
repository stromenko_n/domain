package com.example.core.data.picture.repository;

import com.example.core.data.picture.entity.Picture;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PictureRepository extends MongoRepository<Picture, ObjectId> {
}
