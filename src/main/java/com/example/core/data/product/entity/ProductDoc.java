package com.example.core.data.product.entity;

import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Document
@Data
public class ProductDoc {
    @Id
    private ObjectId id;

    private String name;
    private String description;

    private ObjectId mainPicId;
    private List<ObjectId> pics = new ArrayList<>();
    private ObjectId categoryId;

    private BigDecimal price;

}
