package com.example.core.data.product.exception;

public class ProductException extends Exception {

    public ProductException(String message) {
        super(message);
    }
}
