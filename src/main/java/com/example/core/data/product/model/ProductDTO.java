package com.example.core.data.product.model;

import com.example.core.data.product.entity.ProductDoc;
import com.example.core.domain.utils.id.ObjIdUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductDTO {
    private String id;

    private String name;
    private String description;

    private String mainPicId;
    private List<String> pics;
    private String categoryId;

    private BigDecimal price;

    public static ProductDTO of(ProductDoc doc) {
        return new ProductDTO(ObjIdUtils.objIdToString(doc.getId()),
                doc.getName(),
                doc.getDescription(),
                ObjIdUtils.objIdToString(doc.getMainPicId()),
                doc.getPics().stream()
                        .map(ObjIdUtils::objIdToString)
                        .collect(Collectors.toList()),
                ObjIdUtils.objIdToString(doc.getCategoryId()),
                doc.getPrice());
    }

    public ProductDoc toDoc() {
        ProductDoc productDoc = new ProductDoc();
        if (!StringUtils.isEmpty(id)) {
            productDoc.setId(new ObjectId(id));
        }
        productDoc.setName(name);
        productDoc.setDescription(description);
        if (!StringUtils.isEmpty(mainPicId)) {
            productDoc.setMainPicId(new ObjectId(mainPicId));
        }
        productDoc.setPics(pics.stream()
                .filter(this::idNonNull)
                .map(ObjectId::new)
                .collect(Collectors.toList()));
        if (!StringUtils.isEmpty(categoryId)) {
            productDoc.setCategoryId(new ObjectId(categoryId));
        }
        productDoc.setPrice(price);

        return productDoc;
    }

    public ObjectId transformObjId() {
        return new ObjectId(id);
    }

    private boolean idNonNull(String id) {
        return !StringUtils.isEmpty(id);
    }
}
