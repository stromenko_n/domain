package com.example.core.data.product.repository;

import com.example.core.data.product.entity.ProductDoc;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

public interface ProductRepository {

    String save(ProductDoc doc);

    String delete(ProductDoc doc);

    ProductDoc findById(ObjectId id);

    List<ProductDoc> findAll();

    List<ProductDoc> findByCriteria(Query query);

    void dropCollection();

}
