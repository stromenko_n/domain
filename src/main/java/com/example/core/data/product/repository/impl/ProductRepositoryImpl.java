package com.example.core.data.product.repository.impl;

import com.example.core.data.product.entity.ProductDoc;
import com.example.core.data.product.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@RequiredArgsConstructor
@Repository
public class ProductRepositoryImpl implements ProductRepository {
    private final MongoTemplate mongoTemplate;

    @Override
    public String save(ProductDoc doc) {
        mongoTemplate.save(doc);
        return "OK!";
    }

    @Override
    public String delete(ProductDoc doc) {
        mongoTemplate.remove(doc);
        return "OK!";
    }

    @Override
    public ProductDoc findById(ObjectId id) {
        return mongoTemplate.findById(id, ProductDoc.class);
    }

    @Override
    public List<ProductDoc> findAll() {
        return mongoTemplate.findAll(ProductDoc.class);
    }

    @Override
    public List<ProductDoc> findByCriteria(Query query) {
        return mongoTemplate.find(query, ProductDoc.class);
    }

    @Override
    public void dropCollection() {
        mongoTemplate.dropCollection(ProductDoc.class);
    }
}
