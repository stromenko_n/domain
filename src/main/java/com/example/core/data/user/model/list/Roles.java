package com.example.core.data.user.model.list;

public enum Roles {
    NOT_AUTHORIZED,
    USER,
    MODERATOR,
    ADMIN;
}
