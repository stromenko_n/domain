package com.example.core.data.user.repository;

import com.example.core.data.user.entity.UserDoc;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface UserRepository extends MongoRepository<UserDoc, ObjectId> {
    Optional<UserDoc> findByUsername(String username);
}
