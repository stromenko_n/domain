package com.example.core.domain.enititys.category.service;

import com.example.core.data.category.exception.CategoryException;
import com.example.core.data.category.model.CategoryDTO;
import org.bson.types.ObjectId;

import java.util.List;

public interface CategoryService {

    List<CategoryDTO> findAll();

    CategoryDTO findById(ObjectId id) throws CategoryException;

    String create(CategoryDTO dto);

    String update(CategoryDTO dto) throws CategoryException;

    String delete(ObjectId id) throws CategoryException;

    void dropCollection();
}
