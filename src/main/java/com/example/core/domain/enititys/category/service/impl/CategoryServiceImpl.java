package com.example.core.domain.enititys.category.service.impl;

import com.example.core.data.category.entity.CategoryDoc;
import com.example.core.data.category.exception.CategoryException;
import com.example.core.data.category.model.CategoryDTO;
import com.example.core.data.category.repository.CategoryRepository;
import com.example.core.domain.enititys.category.service.CategoryService;
import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CategoryServiceImpl implements CategoryService {
    private final CategoryRepository repository;

    @Override
    public List<CategoryDTO> findAll() {
        List<CategoryDoc> categoryDocs = repository.findAll();

        return categoryDocs.stream()
                .map(CategoryDTO::of)
                .collect(Collectors.toList());
    }

    @Override
    public CategoryDTO findById(ObjectId id) throws CategoryException {
        CategoryDoc category = repository.findById(id);

        if (Objects.isNull(category)) {
            throw new CategoryException("Category not found");
        }

        return CategoryDTO.of(category);
    }

    @Override
    public String create(CategoryDTO dto) {
        repository.save(dto.toDoc());
        return "OK!";
    }

    @Override
    public String update(CategoryDTO dto) throws CategoryException {
        if (Objects.isNull(repository.findById(dto.getObjId()))) {
            throw new CategoryException("Category not found");
        }
        repository.save(dto.toDoc());
        return "OK!";
    }

    @Override
    public String delete(ObjectId id) throws CategoryException {
        CategoryDoc category = repository.findById(id);
        if (Objects.isNull(category)) {
            throw new CategoryException("Category not found");
        }
        repository.delete(category);
        return "OK!";
    }

    @Override
    public void dropCollection() {
        repository.dropCollection();
    }
}
