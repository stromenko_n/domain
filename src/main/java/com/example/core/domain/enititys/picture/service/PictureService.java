package com.example.core.domain.enititys.picture.service;

import com.example.core.data.picture.entity.Picture;
import com.example.core.data.picture.exception.PictureException;
import org.bson.types.ObjectId;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface PictureService {

    String addPicture(String title, MultipartFile file) throws IOException;

    Picture getPicture(ObjectId id) throws PictureException;
}
