package com.example.core.domain.enititys.picture.service.impl;

import com.example.core.data.picture.entity.Picture;
import com.example.core.data.picture.exception.PictureException;
import com.example.core.data.picture.repository.PictureRepository;
import com.example.core.domain.enititys.picture.service.PictureService;
import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class PictureServiceImpl implements PictureService {
    private final PictureRepository pictureRepository;

    @Override
    public String addPicture(String title, MultipartFile file) throws IOException {
        Picture picture = Picture.of(title, file);
        pictureRepository.insert(picture);

        if (Objects.nonNull(picture.getId())) {
            return picture.getId().toString();
        }
        return "";
    }

    @Override
    public Picture getPicture(ObjectId id) throws PictureException {
        Optional<Picture> picture = pictureRepository.findById(id);

        if (!picture.isPresent()) {
            throw new PictureException("Picture not found");
        }

        return picture.get();
    }
}
