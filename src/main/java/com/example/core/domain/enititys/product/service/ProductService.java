package com.example.core.domain.enititys.product.service;

import com.example.core.data.product.exception.ProductException;
import com.example.core.data.product.model.ProductDTO;
import org.bson.types.ObjectId;

import java.util.List;

public interface ProductService {

    List<ProductDTO> findByCategory(ObjectId categoryId);

    List<ProductDTO> findAllProducts();

    ProductDTO findById(ObjectId id) throws ProductException;

    String create(ProductDTO dto);

    String update(ProductDTO dto) throws ProductException;

    String delete(ObjectId id) throws ProductException;

    void dropCollection();

}
