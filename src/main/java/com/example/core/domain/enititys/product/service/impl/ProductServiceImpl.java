package com.example.core.domain.enititys.product.service.impl;

import com.example.core.data.product.entity.ProductDoc;
import com.example.core.data.product.exception.ProductException;
import com.example.core.data.product.model.ProductDTO;
import com.example.core.data.product.repository.ProductRepository;
import com.example.core.domain.enititys.product.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {
    private final ProductRepository repository;

    @Override
    public List<ProductDTO> findByCategory(ObjectId categoryId) {
        List<ProductDoc> productDocs = repository.findByCriteria(new Query(
                Criteria.where("categoryId").is(categoryId)));

        return productDocs.stream()
                .map(ProductDTO::of)
                .collect(Collectors.toList());
    }

    @Override
    public List<ProductDTO> findAllProducts() {
        List<ProductDoc> productDocs = repository.findAll();

        return productDocs.stream()
                .map(ProductDTO::of)
                .collect(Collectors.toList());
    }

    @Override
    public ProductDTO findById(ObjectId id) throws ProductException {
        ProductDoc productDoc = repository.findById(id);
        if (Objects.isNull(productDoc)) {
            throw new ProductException("Product not found");
        }
        return ProductDTO.of(productDoc);
    }

    @Override
    public String create(ProductDTO dto) {
        repository.save(dto.toDoc());
        return "OK!";
    }

    @Override
    public String update(ProductDTO dto) throws ProductException {
        if (Objects.isNull(repository.findById(dto.transformObjId()))) {
            throw new ProductException("Product not found");
        }
        repository.save(dto.toDoc());
        return "OK!";
    }

    @Override
    public String delete(ObjectId id) throws ProductException {
        ProductDoc productDoc = repository.findById(id);
        if (Objects.isNull(productDoc)) {
            throw new ProductException("Product not found");
        }
        repository.delete(productDoc);
        return "OK!";
    }

    @Override
    public void dropCollection() {
        repository.dropCollection();
    }
}
