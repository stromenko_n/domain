package com.example.core.domain.enititys.user.service;

import com.example.core.data.user.entity.UserDoc;
import com.example.core.data.user.model.Role;
import com.example.core.data.user.model.list.Roles;
import com.example.core.data.user.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserService implements UserDetailsService {
    private final UserRepository userRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<UserDoc> user = userRepository.findByUsername(username);

        if (user.isPresent()) {
            return user.get();
        }
        throw new UsernameNotFoundException("User not found");
    }

    public UserDoc findUserById(ObjectId userId) {
        Optional<UserDoc> userFromDb = userRepository.findById(userId);
        return userFromDb.orElse(new UserDoc());
    }

    public List<UserDoc> allUsers() {
        return userRepository.findAll();
    }

    public boolean saveUser(UserDoc user) {
        if (userRepository.findByUsername(user.getUsername()).isPresent()) {
            return false;
        }

        user.setRoles(Collections.singleton(new Role(Roles.USER.toString())));
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userRepository.save(user);
        return true;
    }

    public boolean deleteUser(ObjectId userId) {
        if (userRepository.findById(userId).isPresent()) {
            userRepository.deleteById(userId);
            return true;
        }
        return false;
    }
}
