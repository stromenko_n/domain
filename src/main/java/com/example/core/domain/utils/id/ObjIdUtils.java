package com.example.core.domain.utils.id;

import org.bson.types.ObjectId;

import java.util.Objects;

public class ObjIdUtils {

    public static String objIdToString(ObjectId id) {
        if (Objects.nonNull(id)) {
            return id.toString();
        } else {
            return "null";
        }
    }
}
